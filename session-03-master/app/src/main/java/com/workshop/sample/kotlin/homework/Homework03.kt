package com.workshop.sample.kotlin.homework

fun getCityThatMostCustomersAreFrom(var listcustomer): City? {
    var max = 0
    for(x in listcustomer){
        x.city.count = x.city.count + 1
    }
    for(x in listcustomer){
        if(max <= x.city.count){
            max = x.city.count
        }
    }
    return x.city.name
}

fun getCustomerWithMaximumNumberOfOrders(var listcustomer): Customer? {
    for(x in listcustomer){
        var max = 0
        var maxcustomer = 0
        if(max <= x.orders.size){
            max = x.orders.size
            maxcustomer = x
        }
    }
    return maxcustomer
}
